This repo contains some code we ported over from projects in the past.  Most of the old code was... like fortran and cobol... so a refresh was needed:

1. dtmf_dec.py - A DTMF decoder for our various projects.  Expects a data rate of 44,100.  Mono audio is preferred.
2. img_dec.py - Still working.  Expects a rate of 192,000.  Mono audio.
3. freq_finder.py - Still working.  Expects a rate of 192,000.  Mono audio.